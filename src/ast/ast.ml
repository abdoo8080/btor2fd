open Format
open Exp

type sort = Bool of bool | Bitvec of num | Array of sid * sid

(* type, length *)
and sid =
  | Sid of int
  (* Sort reference Id *)
  | Sort of int * sort

type ptype = Bad | Constraint | Output

type const =
  | One of sid
  | Ones of sid
  | Zero of sid
  | Constbin of sid * bool
  | Constdec of sid * uint (* unsigned integer *)
  | Consthex of sid * uint

(* change to hex*)

type node =
  | Input of sid * string option
  | State of sid * string option
  | Init of sid * nid * nid
  | Next of sid * nid * nid
  | Exp of exp (* Expression Node *)
  | Const of const (* Constant Node *)
  | Property of ptype * nid

(* Property Node *)
and exp =
  | Uop of uop * sid * nid
  | Bop of bop * sid * nid * nid
  | Top of top * sid * nid * nid * nid
  | Idx of opidx * sid * nid * uint * uint option

and nid =
  | Nid of int
  (* Node reference Id *)
  | Node of int * node

type btor = Nodes of nid list | Sorts of sid list

(* Pretty Printing *)
let pp_string ppf s : unit -> unit = fun _ -> fprintf ppf s

let pp_nl ppf = pp_string ppf "@\n"

let pp_bool ppf (b : bool) : unit = fprintf ppf "%b" b

let pp_int ppf (i : int) : unit = fprintf ppf "%i" i

let pp_str ppf (s : string) : unit = fprintf ppf "%s" s

let pp_opt_str ppf opt : unit =
  match opt with Some v -> fprintf ppf "%a" pp_str v | None -> ()

let pp_opt_uint ppf opt : unit =
  match opt with Some v -> fprintf ppf "%a" pp_int v | None -> ()

let pp_num ppf (i : num) : unit = fprintf ppf "%i" i

let rec pp_sort ppf (s : sort) =
  match s with
  | Bool b -> fprintf ppf "Boolean %b" b
  | Bitvec n -> fprintf ppf "Sort BV(%i)" n
  | Array (s1, s2) -> fprintf ppf "Sort Array (%a,%a)" pp_sid s1 pp_sid s2

and pp_sid ppf (ty : sid) =
  match ty with
  | Sid i -> pp_int ppf i
  | Sort (i, s) -> fprintf ppf "%a %a" pp_int i pp_sort s

let pp_sid_sort ppf (ty : sid) =
  match ty with
  | Sid i -> pp_int ppf i
  | Sort (i, s) -> fprintf ppf "%a %a" pp_int i pp_sort s

let pp_uop ppf (op : uop) =
  match op with Not -> fprintf ppf "NOT" | Neg -> fprintf ppf "NEG" | _ -> fprintf ppf "Add Later"

let pp_bop ppf (op : bop) =
  match op with
  | And -> fprintf ppf "AND"
  | Eq -> fprintf ppf "EQ"
  | Neq -> fprintf ppf "NEQ"
  | Add -> fprintf ppf "ADD"
  | _ -> fprintf ppf "Add Later"

let pp_top ppf (op : top) =
  match op with Ite -> fprintf ppf "ITE" | Write -> fprintf ppf "WRITE"

let pp_idx ppf (op : opidx) =
  match op with
  | Slice -> fprintf ppf "SLICE"
  | Uext -> fprintf ppf "UEXT"
  | Sext -> fprintf ppf "SEXT"

let pp_const ppf (c : const) =
  match c with
  | One sid -> fprintf ppf "One <%a>" pp_sid sid
  | Ones sid -> fprintf ppf "Ones <%a>" pp_sid sid
  | Zero sid -> fprintf ppf "Zero <%a>" pp_sid sid
  | Constbin (s, i) -> fprintf ppf "Constant <%a> %a" pp_sid s pp_bool i
  | Constdec (s, i) -> fprintf ppf "Constant <%a> %a" pp_sid s pp_int i
  | Consthex (s, i) -> fprintf ppf "Constant <%a> %a" pp_sid s pp_int i

let pp_property ppf (p : ptype) =
  match p with
  | Bad -> fprintf ppf "BAD"
  | Constraint -> fprintf ppf "INV"
  | Output -> fprintf ppf "OUTPUT"

let rec pp_node ppf (n : node) =
  match n with
  | Input (s, id) -> fprintf ppf "Input <%a> %a" pp_sid s pp_opt_str id
  | State (s, id) -> fprintf ppf "State <%a> %a" pp_sid s pp_opt_str id
  | Init (s, n1, n2) ->
      fprintf ppf "Init <%a> [%a] [%a]" pp_sid s pp_nid n1 pp_nid n2
  | Next (s, n1, n2) ->
      fprintf ppf "Next <%a> [%a] [%a]" pp_sid s pp_nid n1 pp_nid n2
  | Exp e -> fprintf ppf "%a" pp_exp e
  | Const c -> fprintf ppf "%a" pp_const c
  | Property (p, n1) -> fprintf ppf "%a [%a]" pp_property p pp_nid n1

and pp_nid ppf (n : nid) =
  match n with
  | Nid i -> fprintf ppf "%a" pp_num i
  | Node (i, n1) -> fprintf ppf "%a %a" pp_num i pp_node n1

and pp_exp ppf (e : exp) =
  match e with
  | Uop (op1, s, n1) -> fprintf ppf "%a <%a> %a" pp_uop op1 pp_sid s pp_nid n1
  | Bop (op2, s, n1, n2) ->
      fprintf ppf "%a <%a> [%a] [%a]" pp_bop op2 pp_sid s pp_nid n1 pp_nid n2
  | Top (op3, s, n1, n2, n3) ->
      fprintf ppf "%a <%a> [%a] [%a] [%a]" pp_top op3 pp_sid s pp_nid n1 pp_nid n2
        pp_nid n3
  | Idx (opid, s, n, u1, u2) ->
      fprintf ppf "%a <%a> [%a] [%a] [%a]" pp_idx opid pp_sid s pp_nid n pp_int u1
        pp_opt_uint u2

let rec pp_sortlist ppf slist =
  match slist with
  | [] -> pp_nl
  | [ h ] ->
      fprintf ppf "%a" pp_sid_sort h;
      pp_sortlist ppf []
  | h :: t ->
      fprintf ppf "%a\n" pp_sid_sort h;
      pp_sortlist ppf t

let rec pp_nodelist ppf nlist =
  match nlist with
  | [] -> pp_nl
  | [ h ] ->
      fprintf ppf "%a" pp_nid h;
      pp_nodelist ppf []
  | h :: t ->
      fprintf ppf "%a\n" pp_nid h;
      pp_nodelist ppf t

let pp_prog ppf (p : btor) =
  match p with
  | Nodes nlist ->
      fprintf ppf "----\n";
      pp_nodelist ppf nlist
  | Sorts slist ->
      fprintf ppf "";
      pp_sortlist ppf slist
