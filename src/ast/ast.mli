open Exp

type sort = Bool of bool | Bitvec of int | Array of sid * sid

(* type, length // 'sid * 'sid *)
and sid =
  | Sid of int
  (* Sort Reference ID *)
  | Sort of int * sort

type ptype = Bad | Constraint | Output

type const =
  | One of sid
  | Ones of sid
  | Zero of sid
  | Constbin of sid * bool
  | Constdec of sid * uint (* unsigned integer *)
  | Consthex of sid * uint

(* change to hex*)

type node =
  | Input of sid * string option
  | State of sid * string option
  | Init of sid * nid * nid
  | Next of sid * nid * nid
  | Exp of exp (* Expression Node *)
  | Const of const (* Constant Node *)
  | Property of ptype * nid

(* Property Node *)
and exp =
  | Uop of uop * sid * nid
  | Bop of bop * sid * nid * nid
  | Top of top * sid * nid * nid * nid
  | Idx of opidx * sid * nid * uint * uint option

and nid =
  | Nid of int
  (* Node Reference ID *)
  | Node of int * node

type btor = Nodes of nid list | Sorts of sid list

val pp_string :
  Format.formatter -> (unit, Format.formatter, unit) format -> unit -> unit

val pp_nl : Format.formatter -> unit -> unit

val pp_bool : Format.formatter -> bool -> unit

val pp_int : Format.formatter -> int -> unit

val pp_str : Format.formatter -> string -> unit

val pp_opt_str : Format.formatter -> string option -> unit

val pp_opt_uint : Format.formatter -> int option -> unit

val pp_num : Format.formatter -> num -> unit

val pp_sort : Format.formatter -> sort -> unit

val pp_sid : Format.formatter -> sid -> unit

val pp_sid_sort : Format.formatter -> sid -> unit

val pp_uop : Format.formatter -> uop -> unit

val pp_bop : Format.formatter -> bop -> unit

val pp_top : Format.formatter -> top -> unit

val pp_idx : Format.formatter -> opidx -> unit

val pp_const : Format.formatter -> const -> unit

val pp_property : Format.formatter -> ptype -> unit

val pp_node : Format.formatter -> node -> unit

val pp_nid : Format.formatter -> nid -> unit

val pp_exp : Format.formatter -> exp -> unit

val pp_sortlist :
  Format.formatter -> sid list -> Format.formatter -> unit -> unit

val pp_nodelist :
  Format.formatter -> nid list -> Format.formatter -> unit -> unit

val pp_prog : Format.formatter -> btor -> Format.formatter -> unit -> unit