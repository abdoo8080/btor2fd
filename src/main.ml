open Format
open Ast
open Parsing

(* 
    Btor2 overflow-example progrm: 
    a) sid = Sort 
    b) nid = Node 
*)

let s1 = Sort (1, Bitvec 1)

let s2 = Sort (2, Bitvec 8)

let a1 = Sort (3, Array (s1, s2))

(* Exmaple Program evaluated with unrolled sort and nodes  *)
let n3 = Node (3, Input (s1, Some "Turn"))

let n4 = Node (4, Const (Zero s2))

let n5 = Node (5, State (s2, Some "a"))

let n6 = Node (6, State (s2, Some "b"))

let n7 = Node (7, Init (s2, n5, n4))

let n8 = Node (8, Init (s2, n6, n4))

let n9 = Node (9, Const (Constdec (s2, 3)))

let n10 = Node (10, Exp (Bop (Add, s2, n5, n9)))

let n11 = Node (11, Exp (Bop (Add, s2, n6, n9)))

let n12 = Node (12, Exp (Top (Ite, s2, n3, n5, n10)))

(* No reference id *)
let nn3 = Node (999, Exp (Uop (Neg, s2, n3)))

let n13 = Node (13, Exp (Top (Ite, s2, nn3, n6, n10)))

let n14 = Node (14, Next (s2, n5, n12))

let n15 = Node (15, Next (s2, n5, n13))

let n16 = Node (16, Const (Constdec (s2, 2)))

let n17 = Node (17, Exp (Bop (Eq, s1, n5, n16)))

let n18 = Node (18, Exp (Bop (Eq, s1, n6, n16)))

let n19 = Node (18, Exp (Bop (Add, s1, n17, n18)))

let n20 = Node (20, Property (Bad, n19))

let btor2_psorts = Sorts [ s1; s2; a1 ]

let btor2_pnodes =
  Nodes
    [
      n3;
      n4;
      n5;
      n6;
      n7;
      n8;
      n9;
      n10;
      n11;
      n12;
      n13;
      n14;
      n15;
      n16;
      n17;
      n18;
      n19;
      n20;
    ]

let print_program = pp_prog std_formatter

let () = (print_program btor2_psorts) std_formatter ()

let () = (print_program btor2_pnodes) std_formatter ()

(* Same Program with sort and node Ids  *)

let n3 = Node (3, Input (Sid 1, Some "Turn"))

let n4 = Node (4, Const (Zero (Sid 2)))

let n5 = Node (5, State (Sid 2, Some "a"))

let n6 = Node (6, State (Sid 2, Some "b"))

let a1 = Sort (3, Array (s1, s2))

let n7 = Node (7, Init (Sid 2, Nid 5, Nid 4))

let n8 = Node (8, Init (Sid 2, Nid 6, Nid 4))

let n9 = Node (9, Const (Constdec (Sid 2, 3)))

let n10 = Node (10, Exp (Bop (Add, Sid 2, Nid 5, Nid 9)))

let n11 = Node (11, Exp (Bop (Add, Sid 2, Nid 6, Nid 9)))

let n12 = Node (12, Exp (Top (Ite, Sid 2, Nid 3, Nid 5, Nid 10)))

(* No reference id *)
let nn3 = Node (999, Exp (Uop (Neg, Sid 2, Nid 3)))

let n13 = Node (13, Exp (Top (Ite, Sid 2, nn3, Nid 6, Nid 10)))

let n14 = Node (14, Next (Sid 2, Nid 5, Nid 12))

let n15 = Node (15, Next (Sid 2, Nid 5, Nid 13))

let n16 = Node (16, Const (Constdec (Sid 2, 2)))

let n17 = Node (17, Exp (Bop (Eq, Sid 1, Nid 5, Nid 16)))

let n18 = Node (18, Exp (Bop (Eq, Sid 1, Nid 6, Nid 16)))

let n19 = Node (18, Exp (Bop (Add, Sid 1, Nid 17, Nid 18)))

let n20 = Node (20, Property (Bad, Nid 19))

let a2 = Sort (3, Array (Sid 1, Sid 2))

let btor_prog_sort1 = Sorts [ s1; s2; a2 ]

let btor_prog_nodes1 =
  Nodes
    [
      n3;
      n4;
      n5;
      n6;
      n7;
      n8;
      n9;
      n10;
      n11;
      n12;
      n13;
      n14;
      n15;
      n16;
      n17;
      n18;
      n19;
      n20;
    ]

let () = fprintf std_formatter "=========================\n"

let () = (print_program btor_prog_sort1) std_formatter ()

let () = (print_program btor_prog_nodes1) std_formatter ()

let b = btor2_psorts = btor_prog_sort1

let b1 = btor2_pnodes = btor_prog_nodes1


let () = fprintf std_formatter "=========================\n"

let p = "
        1 sort bitvec 3
        2 zero 1
        3 state 1 A
        4 init 1 3 2
        5 one 1
        6 add 1 3 5
        7 next 1 3 6
        8 ones 1
        9 sort bitvec 1
        10 eq 9 3 8
        11 bad 10         
        "
let n = Parser.main Lexer.token (Lexing.from_string p) 
let () = (print_program n) std_formatter ()