%{ 
    open Ast 
%}

%token <int> Int
%token <string> ID

(* node *)
%token STATE INIT NEXT

(* I/O *)
%token INPUT OUTPUT

(* properties under consideration *)
%token CONSTRAINT BAD (*FAIR JUSTICE*)

(* Type definitions *)
%token SORT BITVEC ARRAY

(* constants *)
%token ZERO ONE ONES 
%token <bool> CONSTBIN 
%token <int> CONSTDEC 
%token <int> CONSTHEX

(* array operations *)
%token WRITE READ

(* arithmetic operations *)
%token ADD MUL SUB UDIV UREM
%token SDIV SMOD SREM

(* Unsigned/Signed inequality *) 
%token UGT UGTE ULT ULTE 
%token SGT SGTE SLT SLTE

(* Boolean *)
%token NOT AND NAND NOR OR XNOR XOR

%token NEQ EQ IMPLIES IFF 
%token ITE

%token NEG
%token REDAND REDOR REDXOR  

%token ROL ROR SLL SRA SRL

%token SLICE UEXT SEXT CONCAT

(* Overflow *)
%token UADDO SADDO UDIVO SDIVO UMULO SMULO USUBO SSUBO

%token EOF


%start main
%type <Ast.btor> main

%%

main: b = btor EOF {b} (*Nodes bp*)

btor: ss = sorts { Sorts ss }
    | ns = nodes { Nodes ns }

sorts: s = sort { [s] } 
    | s = sort t = sorts { s :: t } 

sort: sid = Int SORT BITVEC num = Int { Sort ( sid, Bitvec num) }
    | sid = Int SORT ARRAY s1 = Int s2 = Int { Sort ( sid, Array ( Sid s1, Sid s2)) }
(*    | sid = Int BOOL b = Int { Bool bool_of_int b} *)

nodes: n = node {[n]}
    | n = node m = nodes { n :: m }

node: nid = Int INPUT sid = Int id = ID { Node(nid, Input(Sid sid, Some id)) } (*later deal with ID option*)
    | nid = Int STATE sid = Int id = ID { Node(nid, State(Sid sid, Some id)) } (*later deal with ID option*)
    | nid = Int INIT sid = Int n1 = Int n2 = Int { Node(nid, Init(Sid sid, Nid n1, Nid n2)) } (*later deal with ID option*)
    | nid = Int NEXT sid = Int n1 = Int n2 = Int { Node(nid, Next(Sid sid, Nid n1, Nid n2)) } (*later deal with ID option*)
    | nid = Int e = exp { Node(nid, Exp(e)) }
    | nid = Int c = const { Node(nid, Const(c)) }
    | nid = Int p = property n = Int {Node (nid, Property(p, Nid n))}

 exp: op = uop sid = Int nid = Int { Uop(op, Sid sid, Nid nid) }
    | op = bop sid = Int n1 = Int n2 = Int { Bop(op, Sid sid, Nid n1, Nid n2) }
    | op = top sid = Int n1 = Int n2 = Int n3 = Int { Top(op, Sid sid, Nid n1, Nid n2, Nid n3) }
    | op = opidx sid = Int n1 = Int uint1 = Int uint2 = Int { Idx(op, Sid sid, Nid n1, uint1, Some uint2) }

 const: ONE sid = Int { One(Sid sid) }
    | ONES sid = Int { Ones(Sid sid) }
    | ZERO sid = Int { Zero(Sid sid) }
    | CONSTBIN sid = Int  { Constbin(Sid sid, true) } (* Correct default boolean *)
    | CONSTDEC sid = Int d = Int { Constdec(Sid sid, d) }
    | CONSTHEX sid = Int h = Int { Consthex(Sid sid, h) } (* Correct Hex case *)

 property: BAD { Bad }
    | CONSTRAINT { Constraint }
    | OUTPUT { Output }

 uop: NOT { Not }
    | NEG { Neg }
    | REDAND { Redand }
    | REDOR { Redor }
    | REDXOR  {Redxor } 

 bop: AND { And }
    | NAND { Nand }
    | NOR { Nor }
    | OR { Or }
    | XOR { Xor }
    | XNOR { Xnor }
    | IMPLIES { Implies}
    | IFF { Iff }

    | EQ { Eq }
    | NEQ { Neq }

    | UGT { Ugt }
    | UGTE {Ugte}
    | SGT { Sgt }
    | SGTE {Sgte}
    | ULT { Ult }
    | SLT { Slt }
    | ULTE { Ulte }
    | SLTE { Slte }

    | ROL { Rol }
    | ROR { Ror }
    | SLL { Sll }
    | SRA { Sra }
    | SRL { Srl }

    | ADD { Add }
    | SUB { Sub }
    | MUL { Mul }
    | UDIV { Udiv }
    | SDIV { Sdiv }
    | SMOD { Smod }
    | UREM { Urem }
    | SREM { Srem}

    | UADDO { Uaddo }
    | SADDO { Saddo }
    | UDIVO { Udivo }
    | SDIVO { Sdivo }
    | UMULO { Umulo }
    | SMULO { Smulo }
    | USUBO { Usubo }
    | SSUBO { Ssubo}
    
    | READ { Read }
    | CONCAT { Concat }

 top: ITE { Ite }
    | WRITE { Write }

 opidx: SLICE { Slice }
    | UEXT { Uext }
    | SEXT { Sext }