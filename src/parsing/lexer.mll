{
  open Lexing
  open Parser
  
 (* open Printf *)

  let get = Lexing.lexeme

  exception Error of string
  let error fmt = Printf.kprintf (fun msg -> raise (Error msg)) fmt

  let next_line lexbuf =
    let pos = lexbuf.lex_curr_p in
    lexbuf.lex_curr_p <-
      { pos with pos_bol = pos.pos_cnum;
                 pos_lnum = pos.pos_lnum + 1;
      }

  let keyword_table = Hashtbl.create 20
  let _ =
    List.iter (fun (kwd, tok) -> Hashtbl.add keyword_table kwd tok)
    ["sort", SORT;
		"bitvec", BITVEC;
		"array", ARRAY;
		
(*  "true", BOOL(True);
    "false", BOOL(False); *)

    "init", INIT;
		"next", NEXT;
		"state", STATE;

    "input", INPUT;
		"one", ONE;
		"zero", ZERO;
		"ones", ONES;

 (*   "const", CONSTBIN(True);
		"constd", CONSTDEC;
		"consth", CONSTHEX;
_*)
		"bad", BAD;
		"constraint", CONSTRAINT;
		"output", OUTPUT]
}

let ws = [' ' '\009' '\012']
(*let dos_newline = "\013\010"*)
let newline = '\r' | '\n' | "\r\n"

let digit = ['0'-'9']
let hexdigit = digit | ['a'-'f' 'A'-'F']

let dec = (digit)+
let bin = ('0'|'1')+
let hex = (hexdigit)+

let ident = ('_')* ['a'-'z' 'A'-'Z' '\'' ]['a'-'z' 'A'-'Z' '0'-'9' '\\' '_']*

rule token = parse
  | ident  { ID (get lexbuf) }  
  | (dec as d)  { Int (int_of_string d) }
  | (bin as b)   {CONSTBIN (bool_of_string b)}
  | (hex as h)   {CONSTDEC (int_of_string h)}

  (* FILL IN A BUNCH MORE RULES HERE *)		    
  | "sort" {SORT}        (* Sort Type *) 
  | "bitvec" {BITVEC}    (* BitVector / Register *)
  | "array" {ARRAY}      (* Array / Memory *)

  | "state" {STATE}
  | "input" {INPUT}    
  | "init" {INIT}        (* Initialization *)
  | "next" {NEXT}        (* Successor *)

  | "output" {OUTPUT}

  (* Properties *)
  | "bad" {BAD}
  | "constraint" {CONSTRAINT}
(*| "fair" {FAIR}
  | "Output" {OUTPUT}
  | "Justice" {JUSTICE} *)

  (* Constants       
  | "const" {CONSTBIN}        (* Bitvector Constant *)
  | "constd" {CONSTDEC}      (* Bitvector Constant Decimal *)   
  | "consth" {CONSTHEX}      (* Bitvector Constant Hex *) 
  *)
  | "one" {ONE}       (* ? *)
  | "ones" {ONES}     (* Bitvector bv1 *)
  | "zero" {ZERO}     (* Bitvector bv0 *)

  (* ------------------------- *)
  (* [opidx] Indexed operators *)
  (* ------------------------- *)
  | "uext" {UEXT}      (* Unsigned extension *)
  | "sext" {SEXT}      (* signed extension *)    
  | "slice" {SLICE}   (* extraction *)   

  (* -------------------------------- *)
  (* [op] Unary unindexed operators  *)
  (* -------------------------------- *)
  | "not" {NOT}       (* Bit-wise *)
  | "-" {NEG}       (* arithmetic *)

  | "redand" {REDAND}  (* reduction *)
  | "redor" {REDOR}    (* reduction *)
  | "redxor" {REDXOR}  (* reduction *)

  (* -------------------------------- *)
  (* Binary unindexed operators / op  *)
  (* -------------------------------- *)
  | "iff" {IFF}           (* Boolean *)
  | "implies" {IMPLIES}   (* Boolean *)

  | "eq" {EQ}             (* Equality *)
  | "neq" {NEQ}           (* Dis-equality *)

  (* Unsigned/Signed inequality *)
  | "ugt" {UGT}
  | "sgt" {SGT}
  | "ugte" {UGTE}
  | "sgte" {SGTE}
  | "ult" {ULT}
  | "slt" {SLT}
  | "ulte" {ULTE}
  | "slte" {SLTE}

  (* Boolean *)
  | "and" {AND}
  | "nand" {NAND}
  | "nor" {NOR}
  | "or" {OR} 
  | "xnor" {XNOR}
  | "xor" {XOR}

  (* Rotate, Shift *)
  | "rol" {ROL}
  | "ror" {ROR}
  | "sll" {SLL}
  | "sra" {SRA}
  | "srl" {SRL}

  (* Arithmetic *)
  | "add" {ADD}
  | "mul" {MUL}
  | "udiv" {UDIV}
  | "sdiv" {SDIV}
  | "smod" {SMOD}
  | "urem" {UREM}
  | "srem" {SREM}
  | "sub" {SUB}

  (* Overflow *)
  | "uaddo" {UADDO}
  | "saddo" {SADDO}
  | "udivo" {UDIVO}
  | "sdivo" {SDIVO}
  | "umulo" {UMULO}
  | "smulo" {SMULO}
  | "usubo" {USUBO}
  | "ssubo" {SSUBO}

  (* Concatenation *)
  |"concat" {CONCAT}

  (* Array Read *)
  | "read" {READ}

  (* -------------------------- *)
  (* (opidx) Ternary Operators *)
  (* -------------------------- *)
  | "ite" {ITE}      (* Conditional *)
  | "write" {WRITE}  (* Array Write *)

and comment = parse
    newline    { next_line lexbuf; token lexbuf }
  | eof        { EOF }
  |  ";"   { comment lexbuf }
  | ws+    { token lexbuf }
  | _     { error "illegal character" }


(* 
  
BTOR2 Lexer 

| SID (ARRAY or BITVEC)
| NID (INPUT or STATE)
| NID opidx SID NID UNIT
| NID op SID NID
| NID (INIT or NEXT) SID NID NID
| NID (BAD or CONSTRAINT or OUTPUT) NID {};

opidx:
| NID op SID NID

op:
| NID SID SID

special_char_replacements = {'$': '', '\\': '.', ':': '_c_'}

NL = '\n'

SN='N%s'

COM=';'

 *)